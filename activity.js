//CREATE - ADD ATLEAST 5 BAND MEMBERS
db.users.insertMany([
    {
        firstName: "Jay",
        lastName: "Contreras",
        email: "jayC@mail.com",
        password: "jayCpogi",
        isAdmin: false

    },
    {
        firstName: "Jomal",
        lastName: "Linao",
        email: "jomalL@mail.com",
        password: "jomalLpogi",
        isAdmin: false
    },
    {
        firstName: "Led",
        lastName: "Tuyay",
        email: "ledT@mail.com",
        password: "ledTpogi",
        isAdmin: false
    },
    {
        firstName: "Puto",
        lastName: "Astete",
        email: "putoA@mail.com",
        password: "putoApogi",
        isAdmin: false
    },
    {
        firstName: "Bords",
        lastName: "Burdeos",
        email: "bordsB@mail.com",
        password: "bordsBpogi",
        isAdmin: false
    }
])

//CREATE - ADD 3 NEW COURSES
db.newCourses.insertMany([
    {
        name: "MECHANICS 101",
        price: 5000,
        isActive: false
    },
    {
        name: "DYNAMICS 101",
        price: 7500,
        isActive: false
    },
    {
        name: "STATICS 101",
        price: 10000,
        isActive: false
    }
])

//READ - FIND ALL NON-ADMIN USERS
db.users.find({ isAdmin: false })

//UPDATE - FIRST USER TO AN ADMIN
db.users.updateOne({}, { $set: { isAdmin: true } })

//UPDATE - ONE OF THE COURSES TO ACTIVE
db.newCourses.updateOne({ name: "MECHANICS 101" }, { $set: { isActive: true } })

//DELETE - DELETE ALL INACTIVE USERS
db.users.deleteMany({ isAdmin: false })


